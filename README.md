# SimpleStreamer #


### Implementation of the node js ETP server as a simulated simple streamer ###

* This repo contains just the minimal files to deploy the node js ETP server with a configuration that serves the sample log data as an ETP simple streamer. It is currently deployed on Microsoft Azure at wss://simplestreamer.cartewright.com.

* The web.config is specific to Azure and IIS, if you host this elsewhere it is not required.

* You can play with app.js to pass additional parameters to the node server, or change the name of the log file that is played.

### Contact ###

* wsmckenz@cartewright.com