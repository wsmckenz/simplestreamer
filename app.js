var https = require('https');

// Make it look like we called the server with the following:
process.argv.push("--wsPort");
process.argv.push(process.env.PORT);
process.argv.push("--simpleStreamer");
process.argv.push("--storeModule");
process.argv.push("null");
process.argv.push("--authentication");
process.argv.push("jwt"); 
process.argv.push("--providerModule");
process.argv.push("../lib/providers/LogPlayer"); 
process.argv.push("--speed");
process.argv.push("10");  
process.argv.push("--fileName"); 
process.argv.push("./node_modules/etp-server/test/data/log1411.xml");

var etp=require("./node_modules/etp-server/dist/bin/server");

function getToken(req, res) {

    // This is the rest request to the Auth0 token api.
	var options = {
        "async": true,
        "crossDomain": true,
        "host": "cartewright.auth0.com",
        "path": "/oauth/token",
        "method": "POST",
        "headers": {
            "content-type": "application/json"
        },
	}

	// This is json for the body sent to the auth0 token api
	var tokenRequest = {
        client_id:  process.env.JWT_CLIENT_ID,
        client_secret: process.env.JWT_CLIENT_SECRET,
        audience: "simplestreamer",
        grant_type: "client_credentials"
	};


	console.dir(options);
    console.dir(tokenRequest);
	
	var req=https.request(options, function (response) {
		response.setEncoding('utf-8');

		var responseString = '';

		response.on('data', function(data) {
			responseString += data;
		});

		response.on('end', function() {
			// console.log(responseString);
			var responseObject = JSON.parse(responseString);
			return res.json(responseObject);
		});

	}.bind(this));

	req.write(JSON.stringify(tokenRequest));

	req.end();
}

// Registers our token api endpoint with express
etp.use("/api/token", getToken)